<?php

namespace AresliaBundle\Entity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Actiualite
 *
 * @ORM\Table(name="actiualite")
 * @ORM\Entity
 */
class Actiualite
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_Actiualite", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idActiualite;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre", type="string", length=255, nullable=false)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="Descreption", type="string", length=255, nullable=false)
     */
    private $descreption;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date_Validite", type="date", nullable=false)
     */
    private $dateValidite;

    /**
     * @var string
     *
     * @ORM\Column(name="Image", type="string", length=500, nullable=false)
     */
    private $image;


    /**
     * @Assert\File(
     *     maxSize = "500M",
     *
     *     uploadIniSizeErrorMessage = "uploaded file is larger than the upload_max_filesize PHP.ini setting",
     *     uploadFormSizeErrorMessage = "uploaded file is larger than allowed by the HTML file input field",
     *     uploadErrorMessage = "uploaded file could not be uploaded for some unknown reason",
     *     maxSizeMessage = "fichier trop volumineux"
     * )
     */
    public $file;










    function setImage($image) {
        $this->image =$image;
    }


    public function getAbsolutePath() {
        return null === $this->image ? null : $this->getUploadRootDir() . '/' . $this->image;
    }

    public function getWebPath() {
        return null === $this->image ? null : $this->getUploadDir() . '/' . $this->image;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/upload/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return '/Actuialite/';
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile() {
        return $this->file;
    }

    public function upload() {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }
$date = new \DateTime();
        $timestamp = $date->format('U');
        // use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the
        // target filename to move to
        $string = str_replace(' ', '-', $this->titre);
//$filename = "" . $timestamp . basename($file->getClientOriginalName());
        $this->file->move($this->getUploadRootDir(), "" . $timestamp . basename($string));

        // On sauvegarde le nom de fichier
        $this->image = "" . $timestamp . basename($string);;

        // La propriété file ne servira plus
        $this->file = null;
    }


    /**
     * Get idActiualite
     *
     * @return integer
     */
    public function getIdActiualite()
    {
        return $this->idActiualite;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Actiualite
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *$this->titre
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set descreption
     *
     * @param string $descreption
     *
     * @return Actiualite
     */
    public function setDescreption($descreption)
    {
        $this->descreption = $descreption;

        return $this;
    }

    /**
     * Get descreption
     *
     * @return string
     */
    public function getDescreption()
    {
        return $this->descreption;
    }

    /**
     * Set dateValidite
     *
     * @param \DateTime $dateValidite
     *
     * @return Actiualite
     */
    public function setDateValidite($dateValidite)
    {
        $this->dateValidite = $dateValidite;

        return $this;
    }

    /**
     * Get dateValidite
     *
     * @return \DateTime
     */
    public function getDateValidite()
    {
        return $this->dateValidite;
    }


    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
}
