<?php

namespace AresliaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prestationinstitut
 *
 * @ORM\Table(name="prestationinstitut")
 * @ORM\Entity
 */
class Prestationinstitut
{
    /**
     * @var integer
     *
     * @ORM\Column(name="Id_Institut", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idInstitut;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre", type="string", length=255, nullable=false)
     */
    private $titre;



    /**
     * Get idInstitut
     *
     * @return integer
     */
    public function getIdInstitut()
    {
        return $this->idInstitut;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Prestationinstitut
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }
}
