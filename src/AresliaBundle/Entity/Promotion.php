<?php

namespace AresliaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Promotion
 *
 * @ORM\Table(name="promotion", indexes={@ORM\Index(name="fk", columns={"id_categorie_promo"})})
 * @ORM\Entity
 */
class Promotion
{
    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=false)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="Descreption", type="string", length=255, nullable=false)
     */
    private $descreption;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date_Validite", type="date", nullable=true)
     */
    private $dateValidite;

    /**
     * @var float
     *
     * @ORM\Column(name="Pourcentage", type="float", precision=10, scale=0, nullable=false)
     */
    private $pourcentage;

    /**
     * @var string
     *
     * @ORM\Column(name="Image", type="string", length=500, nullable=false)
     */
    private $image;

    /**
     * @var integer
     *
     * @ORM\Column(name="Prix", type="integer", nullable=false)
     */
    private $prix;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_ajout", type="date", nullable=false)
     */
    private $dateAjout;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_Promotion", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPromotion;

    /**
     * @var \AresliaBundle\Entity\CatergoriePromo
     *
     * @ORM\ManyToOne(targetEntity="AresliaBundle\Entity\CatergoriePromo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_categorie_promo", referencedColumnName="idCategrie")
     * })
     */
    private $idCategoriePromo;


    /**
     * @Assert\File(
     *     maxSize = "500M",
     *
     *     uploadIniSizeErrorMessage = "uploaded file is larger than the upload_max_filesize PHP.ini setting",
     *     uploadFormSizeErrorMessage = "uploaded file is larger than allowed by the HTML file input field",
     *     uploadErrorMessage = "uploaded file could not be uploaded for some unknown reason",
     *     maxSizeMessage = "fichier trop volumineux"
     * )
     */
    public $file;










    function setImage($image) {
        $this->image =$image;
    }


    public function getAbsolutePath() {
        return null === $this->image ? null : $this->getUploadRootDir() . '/' . $this->image;
    }

    public function getWebPath() {
        return null === $this->image ? null : $this->getUploadDir() . '/' . $this->image;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/upload/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return '/Promotion/';
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile() {
        return $this->file;
    }

    public function upload() {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }
$date = new \DateTime();
        $timestamp = $date->format('U');
        // use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the
        // target filename to move to

//$filename = "" . $timestamp . basename($file->getClientOriginalName());
        $this->file->move($this->getUploadRootDir(), "" . $timestamp . basename($this->titre));

        // On sauvegarde le nom de fichier
        $this->image = "" . $timestamp . basename($this->titre);


        // La propriété file ne servira plus
        $this->file = null;
    }


    /**
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param string $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * @return string
     */
    public function getDescreption()
    {
        return $this->descreption;
    }

    /**
     * @param string $descreption
     */
    public function setDescreption($descreption)
    {
        $this->descreption = $descreption;
    }

    /**
     * @return \DateTime
     */
    public function getDateValidite()
    {
        return $this->dateValidite;
    }

    /**
     * @param \DateTime $dateValidite
     */
    public function setDateValidite($dateValidite)
    {
        $this->dateValidite = $dateValidite;
    }

    /**
     * @return float
     */
    public function getPourcentage()
    {
        return $this->pourcentage;
    }

    /**
     * @param float $pourcentage
     */
    public function setPourcentage($pourcentage)
    {
        $this->pourcentage = $pourcentage;
    }



    /**
     * @return int
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param int $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    /**
     * @return \DateTime
     */
    public function getDateAjout()
    {
        return $this->dateAjout;
    }

    /**
     * @param \DateTime $dateAjout
     */
    public function setDateAjout($dateAjout)
    {
        $this->dateAjout = $dateAjout;
    }

    /**
     * @return int
     */
    public function getIdPromotion()
    {
        return $this->idPromotion;
    }

    /**
     * @param int $idPromotion
     */
    public function setIdPromotion($idPromotion)
    {
        $this->idPromotion = $idPromotion;
    }

    /**
     * @return CatergoriePromo
     */
    public function getIdCategoriePromo()
    {
        return $this->idCategoriePromo;
    }

    /**
     * @param CatergoriePromo $idCategoriePromo
     */
    public function setIdCategoriePromo($idCategoriePromo)
    {
        $this->idCategoriePromo = $idCategoriePromo;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }


}

