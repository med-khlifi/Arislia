<?php

namespace FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Util\SecureRandom;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

/**
 * User
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="user")
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_user", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $idUser;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_salle", type="integer", nullable=false)
     * @Expose
     */
    private $idSalle;

    /**
     * @var integer
     *
     * @ORM\Column(name="age", type="integer", nullable=false)
     * @Expose
     */
    private $age;
    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_msg", type="integer", nullable=false)
     * @Expose
     */
    private $nbrMsg;
    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_annonce", type="integer", nullable=false)
     * @Expose
     */
    private $nbrAnnonce;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", nullable=false)
     * @Expose
     */
    private $sexe;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_naissance", type="date", nullable=false)
     * @Expose
     */
    private $dateNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", nullable=true)
     * @Expose
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", nullable=false)
     * @Expose
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", nullable=false)
     * @Expose
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", nullable=false)
     * @Expose
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", nullable=false)
     *
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", nullable=false)
     *
     */
    private $password;

    /**
     * @var float
     *
     * @ORM\Column(name="solde", type="float", precision=10, scale=0, nullable=false)
     * @Expose
     */
    private $solde;

    /**
     * @var string
     *
     * @ORM\Column(name="devise", type="string", nullable=false)
     * @Expose
     */
    private $devise;

    /**
     * @var string
     *
     * @ORM\Column(name="abn_type", type="string", nullable=false)
     * @Expose
     */
    private $abnType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="abn_debut", type="date", nullable=false)
     * @Expose
     */
    private $abnDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="abn_fin", type="date", nullable=false)
     * @Expose
     */
    private $abnFin;


    /**
     * @Assert\File(maxSize="2048k")
     * @Assert\Image(mimeTypesMessage="Please upload a valid image.")
     */
    protected $profilePictureFile;

    // for temporary storage
    private $tempProfilePicturePath;

    /**
     * Sets the file used for profile picture uploads
     *
     * @param UploadedFile $file
     * @return object
     */
    public function setProfilePictureFile(UploadedFile $file = null) {
        // set the value of the holder
        $this->profilePictureFile       =   $file;
        // check if we have an old image path
        if (isset($this->photo)) {
            // store the old name to delete after the update
            $this->tempprofilePicturePath = $this->photo;
            $this->photo = null;
        } else {
            $this->photo = 'initial';
        }

        return $this;
    }

    /**
     * Get the absolute path of the photo
     */
    public function getProfilePictureAbsolutePath() {
        return null === $this->photo
            ? null
            : $this->getUploadRootDir().'/'.$this->photo;
    }

    /**
     * Get root directory for file uploads
     *
     * @return string
     */
    protected function getUploadRootDir($type='profilePicture') {
        // the absolute directory path where uploaded
        // documents should be saved
        return "/home/gymmobcotm/www/web/".$this->getUploadDir($type);
    }

    /**
     * Specifies where in the /web directory profile pic uploads are stored
     *
     * @return string
     */
    protected function getUploadDir($type='profilePicture') {
        // the type param is to change these methods at a later date for more file uploads
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/Users';
    }

    /**
     * Get the web path for the user
     *
     * @return string
     */
    public function getWebProfilePicturePath() {

        return '/'.$this->getUploadDir().'/'.$this->getPhoto();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUploadProfilePicture() {
        if (null !== $this->getProfilePictureFile()) {
            // a file was uploaded
            // generate a unique filename
            $filename = $this->generateRandomProfilePictureFilename();
            $this->setPhoto("http://www.gym-mob.com/web/uploads/Users/".$filename.'.'.$this->getProfilePictureFile()->guessExtension());
        }
    }

    /**
     * Generates a 32 char long random filename
     *
     * @return string
     */
    public function generateRandomProfilePictureFilename() {
        $count                  =   0;
        do {
            $generator = new SecureRandom();
            $random = $generator->nextBytes(16);
            $randomString = bin2hex($random);
            $count++;
        }
        while(file_exists($this->getUploadRootDir().'/'.$randomString.'.'.$this->getProfilePictureFile()->guessExtension()) && $count < 50);

        return $randomString;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     *
     * Upload the profile picture
     *
     * @return mixed
     */
    public function uploadProfilePicture() {
        // check there is a profile pic to upload
        if ($this->getProfilePictureFile() === null) {
            return;
        }
        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getProfilePictureFile()->move($this->getUploadRootDir(), $this->getPhoto());

        // check if we have an old image
        if (isset($this->tempProfilePicturePath) && file_exists($this->getUploadRootDir().'/'.$this->tempProfilePicturePath)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->tempProfilePicturePath);
            // clear the temp image path
            $this->tempProfilePicturePath = null;
        }
        $this->profilePictureFile = null;
        if (null !== $this->getProfilePictureFile()) {
            // a file was uploaded
            // generate a unique filename
            $filename = $this->generateRandomProfilePictureFilename();
            $this->setPhoto("http://www.gym-mob.com/web/uploads/Users/".$filename.'.'.$this->getProfilePictureFile()->guessExtension());
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeProfilePictureFile()
    {
        if ($file = $this->getProfilePictureAbsolutePath() && file_exists($this->getProfilePictureAbsolutePath())) {
            unlink($file);
        }
    }

    /**
     * Get the file used for profile picture uploads
     *
     * @return UploadedFile
     */
    public function getprofilePictureFile() {

        return $this->profilePictureFile;
    }





    function getTempProfilePicturePath() {
        return $this->tempProfilePicturePath;
    }



    function getIdUser() {
        return $this->idUser;
    }

    function getIdSalle() {
        return $this->idSalle;
    }

    function getSexe() {
        return $this->sexe;
    }

    function getDateNaissance() {
        return $this->dateNaissance;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function getNumero() {
        return $this->numero;
    }

    function getPhoto() {
        return $this->photo;
    }

    function getNom() {
        return $this->nom;
    }

    function getLogin() {
        return $this->login;
    }

    function getPassword() {
        return $this->password;
    }

    function getSolde() {
        return $this->solde;
    }

    function getDevise() {
        return $this->devise;
    }

    function getAbnType() {
        return $this->abnType;
    }

    function getAbnDebut() {
        return $this->abnDebut;
    }

    function getAbnFin() {
        return $this->abnFin;
    }

    function setIdUser($idUser) {
        $this->idUser = $idUser;
    }

    function setIdSalle($idSalle) {
        $this->idSalle = $idSalle;
    }

    function setSexe($sexe) {
        $this->sexe = $sexe;
    }

    function setDateNaissance(\DateTime $dateNaissance) {
        $this->dateNaissance = $dateNaissance;
    }

    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    function setNumero($numero) {
        $this->numero = $numero;
    }

    function setPhoto($photo) {
        $this->photo = $photo;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setSolde($solde) {
        $this->solde = $solde;
    }

    function setDevise($devise) {
        $this->devise = $devise;
    }

    function setAbnType($abnType) {
        $this->abnType = $abnType;
    }

    function setAbnDebut(\DateTime $abnDebut) {
        $this->abnDebut = $abnDebut;
    }

    function setAbnFin(\DateTime $abnFin) {
        $this->abnFin = $abnFin;
    }

    /**
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param int $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return int
     */
    public function getNbrMsg()
    {
        return $this->nbrMsg;
    }

    /**
     * @param int $nbrMsg
     */
    public function setNbrMsg($nbrMsg)
    {
        $this->nbrMsg = $nbrMsg;
    }

    /**
     * @return int
     */
    public function getNbrAnnonce()
    {
        return $this->nbrAnnonce;
    }

    /**
     * @param int $nbrAnnonce
     */
    public function setNbrAnnonce($nbrAnnonce)
    {
        $this->nbrAnnonce = $nbrAnnonce;
    }



}




