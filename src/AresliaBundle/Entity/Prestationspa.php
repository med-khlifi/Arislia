<?php

namespace AresliaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prestationspa
 *
 * @ORM\Table(name="prestationspa")
 * @ORM\Entity
 */
class Prestationspa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_Spa", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idSpa;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre", type="string", length=255, nullable=false)
     */
    private $titre;



    /**
     * Get idSpa
     *
     * @return integer
     */
    public function getIdSpa()
    {
        return $this->idSpa;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Prestationspa
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }
}
