<?php

namespace AresliaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notificationpromo
 *
 * @ORM\Table(name="notificationpromo", indexes={@ORM\Index(name="id_Promo", columns={"id_Promo"})})
 * @ORM\Entity
 */
class Notificationpromo
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation", type="date", nullable=false)
     */
    private $datecreation;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=500, nullable=false)
     */
    private $message;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AresliaBundle\Entity\Promotion
     *
     * @ORM\ManyToOne(targetEntity="AresliaBundle\Entity\Promotion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_Promo", referencedColumnName="id_Promotion")
     * })
     */
    private $idPromo;

    /**
     * @return \DateTime
     */
    public function getDatecreation()
    {
        return $this->datecreation;
    }

    /**
     * @param \DateTime $datecreation
     */
    public function setDatecreation($datecreation)
    {
        $this->datecreation = $datecreation;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Promotion
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * @param Promotion $idPromo
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;
    }




}

