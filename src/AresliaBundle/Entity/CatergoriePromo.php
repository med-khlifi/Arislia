<?php

namespace AresliaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CatergoriePromo
 *
 * @ORM\Table(name="catergorie_promo")
 * @ORM\Entity
 */
class CatergoriePromo
{
    /**
     * @var string
     *
     * @ORM\Column(name="categrie", type="string", length=100, nullable=false)
     */
    private $categrie;

    /**
     * @var integer
     *
     * @ORM\Column(name="idCategrie", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcategrie;

    /**
     * @return string
     */
    public function getCategrie()
    {
        return $this->categrie;
    }

    /**
     * @param string $categrie
     */
    public function setCategrie($categrie)
    {
        $this->categrie = $categrie;
    }

    /**
     * @return int
     */
    public function getIdcategrie()
    {
        return $this->idcategrie;
    }

    /**
     * @param int $idcategrie
     */
    public function setIdcategrie($idcategrie)
    {
        $this->idcategrie = $idcategrie;
    }


}

