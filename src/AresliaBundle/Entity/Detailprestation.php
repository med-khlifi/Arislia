<?php

namespace AresliaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Detailprestation
 *
 * @ORM\Table(name="detailprestation", indexes={@ORM\Index(name="FK_Spa", columns={"id_spa"}), @ORM\Index(name="FK_Institut", columns={"id_institut"})})
 * @ORM\Entity
 */
class Detailprestation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100, nullable=false)
     */
    private $nom;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", precision=10, scale=0, nullable=false)
     */
    private $prix;

    /**
     * @var float
     *
     * @ORM\Column(name="nbrHeure", type="float", nullable=true)
     */
    private $nbrheure;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrPersonne", type="integer", nullable=true)
     */
    private $nbrpersonne;

    /**
     * @var string
     *
     * @ORM\Column(name="suppliment", type="string", length=100, nullable=true)
     */
    private $suppliment;

    /**
     * @var \AresliaBundle\Entity\Prestationinstitut
     *
     * @ORM\ManyToOne(targetEntity="AresliaBundle\Entity\Prestationinstitut")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_institut", referencedColumnName="Id_Institut")
     * })
     */
    private $idInstitut;

    /**
     * @var \AresliaBundle\Entity\Prestationspa
     *
     * @ORM\ManyToOne(targetEntity="AresliaBundle\Entity\Prestationspa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_spa", referencedColumnName="id_Spa")
     * })
     */
    private $idSpa;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Detailprestation
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return Detailprestation
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @return float
     */
    public function getNbrheure()
    {
        return $this->nbrheure;
    }

    /**
     * @param float $nbrheure
     */
    public function setNbrheure($nbrheure)
    {
        $this->nbrheure = $nbrheure;
    }




    /**
     * Set nbrpersonne
     *
     * @param integer $nbrpersonne
     *
     * @return Detailprestation
     */
    public function setNbrpersonne($nbrpersonne)
    {
        $this->nbrpersonne = $nbrpersonne;

        return $this;
    }

    /**
     * Get nbrpersonne
     *
     * @return integer
     */
    public function getNbrpersonne()
    {
        return $this->nbrpersonne;
    }

    /**
     * Set suppliment
     *
     * @param string $suppliment
     *
     * @return Detailprestation
     */
    public function setSuppliment($suppliment)
    {
        $this->suppliment = $suppliment;

        return $this;
    }

    /**
     * Get suppliment
     *
     * @return string
     */
    public function getSuppliment()
    {
        return $this->suppliment;
    }

    /**
     * Set idInstitut
     *
     * @param \AresliaBundle\Entity\Prestationinstitut $idInstitut
     *
     * @return Detailprestation
     */
    public function setIdInstitut(\AresliaBundle\Entity\Prestationinstitut $idInstitut = null)
    {
        $this->idInstitut = $idInstitut;

        return $this;
    }

    /**
     * Get idInstitut
     *
     * @return \AresliaBundle\Entity\Prestationinstitut
     */
    public function getIdInstitut()
    {
        return $this->idInstitut;
    }

    /**
     * Set idSpa
     *
     * @param \AresliaBundle\Entity\Prestationspa $idSpa
     *
     * @return Detailprestation
     */
    public function setIdSpa(\AresliaBundle\Entity\Prestationspa $idSpa = null)
    {
        $this->idSpa = $idSpa;

        return $this;
    }

    /**
     * Get idSpa
     *
     * @return \AresliaBundle\Entity\Prestationspa
     */
    public function getIdSpa()
    {
        return $this->idSpa;
    }
}
