<?php

namespace AresliaBundle\Controller;

use AresliaBundle\Entity\Actiualite;
use AresliaBundle\Entity\Detailprestation;
use AresliaBundle\Entity\Prestationinstitut;
use AresliaBundle\Entity\Promotion;
use AresliaBundle\Form\PromotionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PrestationIController extends Controller
{

    public function AddPestationAction(Request $request)
    {
        $modele1 = new Prestationinstitut();
        $nom = $request->get('Prestation');
        $modele1->setTitre($nom);
        $em = $this->getDoctrine()->getManager();
        $em->persist($modele1);
        $em->flush();
        return $this->redirect($this->generateUrl('areslia_PestationI'));
    }


    public function AddPestationIAction(Request $request,$id)
    {
        $modele1 = new Detailprestation();
        $nom = $request->get('nom');
        $supplement = $request->get('supplement');
        $supplement2 = $request->get('supplement2');
        $prix = $request->get('prix');
        $em = $this->container->get('doctrine')->getEntityManager();
        $Prestationinstitut = $em->getRepository('AresliaBundle:Prestationinstitut')->find($id);
        $modele1->setIdInstitut($Prestationinstitut);
        $modele1->setNom($nom);
        $modele1->setPrix($prix);
        if ($supplement2!=null)
        $modele1->setSuppliment($supplement."+".$supplement2);
        else
            $modele1->setSuppliment($supplement);
        $em = $this->getDoctrine()->getManager();
        $em->persist($modele1);
        $em->flush();
        return $this->redirect($this->generateUrl('areslia_PestationI'));
    }





    public function ListPrestIAction()
    {

        $response["Prestationinstitut"] = array();
        $restresult = $this->getDoctrine()->getRepository('AresliaBundle:Prestationinstitut')->findAll();

        foreach ($restresult as $value){
            $restresultDetail = $this->getDoctrine()->getRepository('AresliaBundle:Detailprestation')->findBy(array("idInstitut" => $value->getIdInstitut()));
           $institut=array( 'IdInstitut' => $value->getIdInstitut(),'Titre' => $value->getTitre(),'Detail' =>$restresultDetail);
            array_push($response["Prestationinstitut"], $institut);
        }
        return $this->render('AresliaBundle:Prestation:PestationIstitut.html.twig',$response);

    }


    public function supprimePestationIAction($id) {

        $em = $this->container->get('doctrine')->getEntityManager();
        $Actiualite = $em->getRepository('AresliaBundle:Detailprestation')->find($id);
        $em->remove($Actiualite);
        $em->flush();
        return null;
    }



    public function supprimePestationAction($id) {

        $em = $this->container->get('doctrine')->getEntityManager();
        $Detailprestation = $em->getRepository('AresliaBundle:Detailprestation')->findBy(array("idInstitut" => $id));
        foreach ($Detailprestation as $value){
            $em->remove($value);
            $em->flush();
        }
        $Prestationinstitut = $em->getRepository('AresliaBundle:Prestationinstitut')->find($id);
        $em->remove($Prestationinstitut);
        $em->flush();
        return $this->redirect($this->generateUrl('areslia_PestationI'));
    }




    public function modifPestationIAction(Request $request,$id) {


        $em = $this->container->get('doctrine')->getEntityManager();
        $prestation = $em->getRepository('AresliaBundle:Detailprestation')->find($id);
        $nom = $request->get('nom');
        $supplement = $request->get('supplement');
        $prix = $request->get('prix');
        $prestation->setNom($nom);
        $prestation->setPrix($prix);
        $prestation->setSuppliment($supplement);
        $em = $this->getDoctrine()->getManager();
        $em->persist($prestation);
        $em->flush();
        return $this->redirect($this->generateUrl('areslia_PestationI'));
    }

    public function modifPestationINAction($nom,$id) {


        $em = $this->container->get('doctrine')->getEntityManager();
        $prestation = $em->getRepository('AresliaBundle:Prestationinstitut')->find($id);


        $prestation->setTitre($nom);

        $em = $this->getDoctrine()->getManager();
        $em->persist($prestation);
        $em->flush();
        return $this->redirect($this->generateUrl('areslia_PestationI'));
    }

}