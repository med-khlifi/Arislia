<?php

namespace AresliaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AresliaBundle\Entity\Promotion;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use AresliaBundle\Entity\Boutique as User;


class WebServicesController extends FOSRestController
{
    /**
     * Create a Token from the submitted data.<br/>
     *

     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="id", nullable=false, strict=true, description="username.")
     * @RequestParam(name="password", nullable=false, strict=true, description="password.")
     * @RequestParam(name="salt", nullable=false, strict=true, description="salt.")
     *
     * @return View
     */
    public function postMessageAction(ParamFetcher $paramFetcher)
    {
        $view = View::create();
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($paramFetcher->get('username'));
        if (!$user instanceof User) {
            $view->setStatusCode(404)->setData("Data received succesfully but with errors.".$paramFetcher->get('username'));

            return $view;
        }


        return $paramFetcher->get('username');

    }

    /**
     * @Rest\Get("/Promotion")
     */
    public function getPromotionAction()
    {$em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT Descreption as descreption,Date_Validite as date_Validite,Pourcentage as pourcentage,
Image as image, Prix as prix , date_ajout,titre,id_Promotion , c.categrie as categorie
FROM promotion p
join catergorie_promo c on c.idCategrie=p.id_categorie_promo

WHERE Date_Validite > SYSDATE() order by id_Promotion  desc
");


        $statement->execute();
        $results = $statement->fetchAll();
        $restresult = $this->getDoctrine()->getRepository('AresliaBundle:Promotion')->findAll();
        if ($restresult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return  array('Promotion' => $results);
    }

    /**
     * @Rest\Get("/Actiualite")
     */
    public function getActiualiteAction()
    {$em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT descreption as descreption,date_validite as date_Validite,
image as image,  titre,id_actiualite 
FROM actiualite


WHERE 1=1 order by id_actiualite  desc
");


        $statement->execute();
        $results = $statement->fetchAll();
        $restresult = $this->getDoctrine()->getRepository('AresliaBundle:Actiualite')->findAll();
        if ($restresult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return  array('Actualite' => $results);
    }


    /**
     * @Rest\Get("/Boutique")
     */
    public function getBoutiqueAction()
    {
        $restresult = $this->getDoctrine()->getRepository('AresliaBundle:Boutique')->findOneBy(array("id"=>1));
        
        if ($restresult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return  array('Boutique' => $restresult);
    }

    /**
     * @Rest\Get("/Prestationinstitut")
     */
    public function getPrestationinstitutAction()
    { $response["Prestationinstitut"] = array();
        $restresult = $this->getDoctrine()->getRepository('AresliaBundle:Prestationinstitut')->findAll();
        foreach ($restresult as $value){
            $restresultDetail = $this->getDoctrine()->getRepository('AresliaBundle:Detailprestation')->findBy(array("idInstitut" => $value->getIdInstitut()));

            $institut=array( 'IdInstitut' => $value->getIdInstitut(),'Titre' => $value->getTitre(),'Detail' =>$restresultDetail);
            array_push($response["Prestationinstitut"], $institut);
        }

       // $restresult = $this->getDoctrine()->getRepository('AresliaBundle:Prestationinstitut')->findAll();
        if ($restresult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return  $response;
    }



    /**
     * @Rest\Get("/Prestationspa")
     */
    public function getPrestationspaAction()
    { $response["Prestationspa"] = array();
        $restresult = $this->getDoctrine()->getRepository('AresliaBundle:Prestationspa')->findAll();
        foreach ($restresult as $value){
            $restresultDetail = $this->getDoctrine()->getRepository('AresliaBundle:Detailprestation')->findBy(array("idSpa" => $value->getIdSpa()));

            $institut=array( 'IdSpa' => $value->getIdSpa(),'Titre' => $value->getTitre(),'Detail' =>$restresultDetail);
            array_push($response["Prestationspa"], $institut);
        }

        // $restresult = $this->getDoctrine()->getRepository('AresliaBundle:Prestationinstitut')->findAll();
        if ($restresult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return  $response;
    }

}