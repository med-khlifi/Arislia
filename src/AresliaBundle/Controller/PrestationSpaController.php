<?php

namespace AresliaBundle\Controller;

use AresliaBundle\Entity\Actiualite;
use AresliaBundle\Entity\Detailprestation;
use AresliaBundle\Entity\Prestationspa;
use AresliaBundle\Entity\Promotion;
use AresliaBundle\Form\PromotionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PrestationSpaController extends Controller
{
    public function AddPestationSpaAction(Request $request)
    {
        $modele1 = new Prestationspa();
        $nom = $request->get('Prestation');
        $modele1->setTitre($nom);
        $em = $this->getDoctrine()->getManager();
        $em->persist($modele1);
        $em->flush();
        return $this->redirect($this->generateUrl('areslia_PestationSpa'));
    }


    public function AddPestationSpaDAction(Request $request,$id)
    {
        $modele1 = new Detailprestation();
        $nom = $request->get('nom');
        $nbrheure = $request->get('nbrheure');
        $nbrpersonne = $request->get('nbrpersonne');
        $prix = $request->get('prix');
        $em = $this->container->get('doctrine')->getEntityManager();
        $Prestationspa = $em->getRepository('AresliaBundle:Prestationspa')->find($id);
        $modele1->setIdSpa($Prestationspa);
        $modele1->setNom($nom);
        $modele1->setPrix($prix);
        $modele1->setNbrheure($nbrheure);
        $modele1->setNbrpersonne($nbrpersonne);
        $em = $this->getDoctrine()->getManager();
        $em->persist($modele1);
        $em->flush();
        return $this->redirect($this->generateUrl('areslia_PestationSpa'));
    }





    public function ListPrestSpaAction()
    {

        $response["Prestationspa"] = array();
        $restresult = $this->getDoctrine()->getRepository('AresliaBundle:Prestationspa')->findAll();

        foreach ($restresult as $value){
            $restresultDetail = $this->getDoctrine()->getRepository('AresliaBundle:Detailprestation')->findBy(array("idSpa" => $value->getIdSpa()));
            $institut=array( 'IdSpa' => $value->getIdSpa(),'Titre' => $value->getTitre(),'Detail' =>$restresultDetail);
            array_push($response["Prestationspa"], $institut);
        }
        return $this->render('AresliaBundle:Prestation:PestationSpa.html.twig',$response);

    }


    public function supprimePestationSpaDAction($id) {

        $em = $this->container->get('doctrine')->getEntityManager();
        $Actiualite = $em->getRepository('AresliaBundle:Detailprestation')->find($id);
        $em->remove($Actiualite);
        $em->flush();
        return null;
    }



    public function supprimePestationSpaAction($id) {

        $em = $this->container->get('doctrine')->getEntityManager();
        $Detailprestation = $em->getRepository('AresliaBundle:Detailprestation')->findBy(array("idSpa" => $id));
        foreach ($Detailprestation as $value){
            $em->remove($value);
            $em->flush();
        }
        $Prestationspa = $em->getRepository('AresliaBundle:Prestationspa')->find($id);
        $em->remove($Prestationspa);
        $em->flush();
        return $this->redirect($this->generateUrl('areslia_PestationSpa'));
    }




    public function modifPestationSpaDAction(Request $request,$id) {


        $em = $this->container->get('doctrine')->getEntityManager();
        $prestation = $em->getRepository('AresliaBundle:Detailprestation')->find($id);
        $nom = $request->get('nom');
        $nbrheure = $request->get('nbrheure');
        $nbrpersonne = $request->get('nbrpersonne');
        $prix = $request->get('prix');
        $prestation->setNom($nom);
        $prestation->setPrix($prix);
        $prestation->setNbrheure($nbrheure);
        $prestation->setNbrpersonne($nbrpersonne);
        $em = $this->getDoctrine()->getManager();
        $em->persist($prestation);
        $em->flush();
        return $this->redirect($this->generateUrl('areslia_PestationSpa'));
    }

    public function modifPestationSpaAction($nom,$id) {


        $em = $this->container->get('doctrine')->getEntityManager();
        $prestation = $em->getRepository('AresliaBundle:Prestationspa')->find($id);


        $prestation->setTitre($nom);

        $em = $this->getDoctrine()->getManager();
        $em->persist($prestation);
        $em->flush();
        return $this->redirect($this->generateUrl('areslia_PestationI'));
    }


}