<?php

namespace AresliaBundle\Controller;

use AresliaBundle\Entity\Actiualite;
use AresliaBundle\Entity\Promotion;
use AresliaBundle\Form\PromotionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BoutiqueController extends Controller
{
    public function AddActuAction()
    {
       return $this->render('AresliaBundle:Actiualite:AddActiualite.html.twig');
    }

    public function AddActuuAction(Request $request)
    {
        $modele1 = new Actiualite();


       //var_dump($request->files->get('file')->getClientOriginalName());
        $titre = $request->get('titre');
        $descreption = $request->get('descreption');
        $date = $request->get('date');



        $file = $request->get('file');
        $cdate = new \DateTime($date);
       //$timestamp = $date->format('U');
        $newDate = date("Y-m-d", strtotime($date));

        $modele1->setTitre($titre);
     $modele1->setImage("http://127.0.0.1/Arislia/web/assets/images/".$request->files->get('file')->getClientOriginalName());


        $modele1->setDescreption($descreption);


        try {
            foreach ($request->files as $uploadedFile) {
                $modele1->setFile($uploadedFile);
            }
        } catch (Exception $e) {
            var_dump($e);        }
        $modele1->upload();
       // var_dump($cdate);
       $modele1->setDateValidite($cdate);



        $em = $this->getDoctrine()->getManager();
        $em->persist($modele1);
        $em->flush();




        return $this->redirect($this->generateUrl('areslia_Actuu'));
    }


    public function ModifBoutiqueAction(Request $request)
    {
        //$modele1 = new Promotion();


        $modele1 = $this->getDoctrine()->getRepository('AresliaBundle:Boutique')->findOneBy(array('id'=>1));
      //  var_dump($request->files->get('file')->getClientOriginalName());
        $nom = $request->get('nom');
        $detail = $request->get('detail');
        $email = $request->get('email');
        $tel = $request->get('tel');
        $adress = $request->get('adress');
        $rue = $request->get('rue');
        $cite = $request->get('cite');



        $modele1->setNom($nom);
        $modele1->setTel($tel);
        $modele1->setDetail($detail);
        $modele1->setPays($adress);
        $modele1->setRue($rue);
        $modele1->setCite($cite);
        $modele1->setEmail($email);




        $em = $this->getDoctrine()->getManager();
        $em->persist($modele1);
        $em->flush();




        return $this->redirect($this->generateUrl('fos_user_change_password'));
    }
    public function BoutiqueAction()
    {

        $Boutique = $this->getDoctrine()->getRepository('AresliaBundle:Boutique')->findOneBy(array('id'=>1));

        return $this->render('AresliaBundle:Boutique:Boutique.html.twig',array('Boutique' => $Boutique));

    }


    public function supprimeActuAction($id) {

        $em = $this->container->get('doctrine')->getEntityManager();
        $Actiualite = $em->getRepository('AresliaBundle:Actiualite')->find($id);
        $em->remove($Actiualite);
        $em->flush();
        // return new Response("suppression avec succès");


        return $this->redirect($this->generateUrl('areslia_Actuu'));
    }


    public function modifActuAction($id) {


        $em = $this->container->get('doctrine')->getEntityManager();
        $Actiualite = $em->getRepository('AresliaBundle:Actiualite')->find($id);


        return $this->render('AresliaBundle:Actiualite:ModifActiualite.html.twig', array( 'Actiualite' => $Actiualite));
    }

}