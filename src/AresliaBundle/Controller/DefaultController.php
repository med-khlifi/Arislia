<?php

namespace AresliaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $Promotion = $this->getDoctrine()->getRepository('AresliaBundle:Promotion')->findAll();
        $Notification = $this->getDoctrine()->getRepository('AresliaBundle:Notificationpromo')->findAll();


        return $this->render('AresliaBundle:Promtion:ListPromotion.html.twig',array('Promotion' => $Promotion,'Notifications' => $Notification));


    }

}
