<?php

namespace AresliaBundle\Controller;

use AresliaBundle\Entity\CatergoriePromo;
use AresliaBundle\Entity\Notificationpromo;
use AresliaBundle\Entity\Promotion;
use AresliaBundle\Form\PromotionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PromotionController extends Controller
{
    public function AddPromoAction()

    { $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('AresliaBundle:CatergoriePromo')->findAll();
       return $this->render('AresliaBundle:Promtion:AddPromotion.html.twig',array('categories' => $categories));
    }

    public function AddPromotionAction(Request $request)
    {
        $modele1 = new Promotion();
        $CatergoriePromo = new CatergoriePromo();
        $em = $this->getDoctrine()->getManager();
       //var_dump($request->files->get('file')->getClientOriginalName());
        $titre = $request->get('titre');
        $descreption = $request->get('descreption');
        $date = $request->get('date');
        $reduction = $request->get('reduction');
        $prix = $request->get('prix');
        $categorie = $request->get('categorie');

        $file = $request->get('file');
        $cdate = new \DateTime($date);
       //$timestamp = $date->format('U');
        $newDate = date("Y-m-d", strtotime($date));

        $categoriee = $em->getRepository('AresliaBundle:CatergoriePromo')->findBy(array('categrie' => $categorie))[0];
        if(is_null($categoriee)){
            $CatergoriePromo->setCategrie($categorie);
            $em->persist($CatergoriePromo);
            $em->flush();
            $categorieee = $em->getRepository('AresliaBundle:CatergoriePromo')->findBy(array('categrie' => $categorie))[0];
            $modele1->setIdCategoriePromo($categorieee);
        }else{
            $modele1->setIdCategoriePromo($categoriee);
        }
        $modele1->setTitre($titre);
        $modele1->setImage("http://127.0.0.1/Arislia/web/assets/images/".$request->files->get('file')->getClientOriginalName());
        $modele1->setDateAjout($cdate);
        $modele1->setPourcentage($reduction);
        $modele1->setDescreption($descreption);



        try {
            foreach ($request->files as $uploadedFile) {
                $modele1->setFile($uploadedFile);
            }
        } catch (Exception $e) {
            var_dump($e);        }
        $modele1->upload();
       // var_dump($cdate);
       $modele1->setDateValidite($cdate);

        $modele1->setPrix($prix);


        $em->persist($modele1);
        $em->flush();






        return $this->redirect($this->generateUrl('areslia_homepage'));
    }


    public function modifAction(Request $request,$id)
    {
        //$modele1 = new Promotion();
        $CatergoriePromo = new CatergoriePromo();
        $em = $this->container->get('doctrine')->getEntityManager();
        $modele1 = $em->getRepository('AresliaBundle:Promotion')->find($id);
      //  var_dump($request->files->get('file')->getClientOriginalName());
        $titre = $request->get('titre');
        $descreption = $request->get('descreption');
        $date = $request->get('date');
        $reduction = $request->get('reduction');
        $categorie = $request->get('categorie');
        $prix = $request->get('prix');

        $file = $request->get('file');
        $cdate = new \DateTime($date);
        //$timestamp = $date->format('U');
        $newDate = date("Y-m-d", strtotime($date));
        $categoriee = $em->getRepository('AresliaBundle:CatergoriePromo')->findBy(array('categrie' => $categorie))[0];
        if(is_null($categoriee)){
            $CatergoriePromo->setCategrie($categorie);
            $em->persist($CatergoriePromo);
            $em->flush();
            $categorieee = $em->getRepository('AresliaBundle:CatergoriePromo')->findBy(array('categrie' => $categorie))[0];
            $modele1->setIdCategoriePromo($categorieee);
        }else{
            $modele1->setIdCategoriePromo($categoriee);
        }
        $modele1->setTitre($titre);

        $modele1->setPourcentage($reduction);
        $modele1->setDescreption($descreption);


        try {
            foreach ($request->files as $uploadedFile) {
                $modele1->setFile($uploadedFile);
            }
        } catch (Exception $e) {
            var_dump($e);        }
        $modele1->upload();
        //var_dump($cdate);
        $modele1->setDateValidite($cdate);

        $modele1->setPrix($prix);

        $em = $this->getDoctrine()->getManager();
        $em->persist($modele1);
        $em->flush();




        return $this->redirect($this->generateUrl('areslia_homepage'));
    }
    public function ListPromoAction()
    {

        $Promotion = $this->getDoctrine()->getRepository('AresliaBundle:Promotion')->findAll();
        $Notification = $this->getDoctrine()->getRepository('AresliaBundle:Notificationpromo')->findAll();
        var_dump($Promotion);
        return $this->render('AresliaBundle:Promtion:ListPromotion.html.twig',array('Promotions' => $Promotion,'Notifications' => $Notification));

    }


    public function supprimePromoAction($id) {

        $em = $this->container->get('doctrine')->getEntityManager();
        $Promotion = $em->getRepository('AresliaBundle:Promotion')->find($id);
        $em->remove($Promotion);
        $em->flush();
        // return new Response("suppression avec succès");


        return null;
    }


    public function modifPromoAction($id) {


        $em = $this->container->get('doctrine')->getEntityManager();
        $Promotion = $em->getRepository('AresliaBundle:Promotion')->find($id);
        $connection = $em->getConnection();
        $statement1 = $connection->prepare("SELECT *
FROM promotion p
join catergorie_promo c on c.idCategrie=p.id_categorie_promo

WHERE id_Promotion = :id 
");
        $statement1->bindValue('id', $id);
        $statement1->execute();
        $results1 = $statement1->fetch();

        $categories = $em->getRepository('AresliaBundle:CatergoriePromo')->findAll();
        return $this->render('AresliaBundle:Promtion:ModifPromotion.html.twig', array( 'Promotion' => $results1 ,'categories' => $categories));
    }



    function send_notificationAction ($id)
    {
        $tokens='/topics/newsHelloExpress';
        $em = $this->container->get('doctrine')->getEntityManager();
        $Promotion = $em->getRepository('AresliaBundle:Promotion')->find($id);

        $msg=$Promotion->getDescreption();
        $title=$Promotion->getTitre();
        $idPromo=$Promotion->getIdPromotion();
        $qq=$Promotion->getImage();
        $image="http://app.arislia.fr/upload/Promotion/".$qq;

        $data = array(
            "date" => date("Y/m/d"),
            "message" => $msg,
            "title" => "Arislia",
            "image" => $image,
            "id" => $idPromo,
            "content_available"=> true,
            'sound' => 'default',
            'badge' => '1',
            "seen"=>"0"
        );
        $url = 'https://fcm.googleapis.com/fcm/send';
        $notif  = array (
            'body' => $msg,
            'title' => "Arislia Beauté Institut" ,
            'sound' => 'default'
        );
        $fields = array(
            'to' => $tokens,
            'priority' => 'HIGH',
            'notification' => $notif,
            "content_available" => true,
        "data" => array (
        "media" => "saif"
        )

        );

        $headers = array(
            'Authorization:key = AIzaSyCqUuLE_MpI_CUQLtOCe3mQ9Nd0ri0R358',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);


        $Notificationpromo = new Notificationpromo();
        $Notificationpromo->setDatecreation(new \DateTime());
        $Notificationpromo->setIdPromo($Promotion);
        $Notificationpromo->setMessage($msg);
        $em = $this->getDoctrine()->getManager();
        $em->persist($Notificationpromo);
        $em->flush();
        /******************************************
        Intervention HelloExpress : Save to Database Messages HelloExpress.
        Paramaters :
        date("Y/m/d") = Function
        $message,
        $title

         ******************/

        return $result;
    }
}